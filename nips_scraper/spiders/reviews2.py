# -*- coding: utf-8 -*-
import scrapy
from nips_scraper.items import NipsScraperItem, Nips2020ReviewItem

class NipsSpider(scrapy.Spider):
    name = 'reviews'
    allowed_domains = ['papers.neurips.cc', 'papers.nips.cc', 'media.neurips.cc', 'media.nips.cc', 
    'proceedings.neurips.cc']
    start_urls = ['https://proceedings.neurips.cc/']
    root_url = 'https://proceedings.neurips.cc'
    nopdf = 0

    def parse(self, response):
        print("In parse", response)
        links = response.xpath("//div[@class='container-fluid']//li/a/@href").extract()
        for link in links:
            if 'year' in response.meta:
                if link.split("/")[1] != 'paper':
                    continue
                yield scrapy.Request(self.root_url + link, callback = self.get_review_link, meta=response.meta)
            else: 
                year = int(link.split("/")[-1])
                # if year < 2013:
                if year <= 2019:
                    continue
                yield scrapy.Request(self.root_url + link, callback = self.parse, meta={'year': year})

    def get_review_link(self, response):
        print("Getting review link...")
        try:
            review_link = response.xpath("//div[@class='container-fluid']//a[text()='Review »']/@href").extract()[0]
        except:
            print("NO review")
        paper_title = response.xpath("//h4/text()").extract()[0]
        year = response.meta['year']
        filename = "pdfs/I%s-%s.pdf"%(str(year)[2:],paper_title)
        print("REVIEW LINK", self.root_url + "/" + review_link)
        yield scrapy.Request(self.root_url + "/" + review_link, callback = self.extract_reviews, meta={'year': year, 
            'paper_name': paper_title, 'filename': filename})

    def extract_reviews(self, response):
        print("GOT REQUEST")
        reviews = response.xpath(".//p")
        print("REVIEWS", len(reviews))
        assert(len(reviews)>0)
        item = None

        for node in reviews: 
            node_text = node.xpath(".//text()").extract()
            node_text = [t.strip() for t in node_text if t.strip()]
            print(node_text[:3])

            for i, text in enumerate(node_text):
                if text.strip().startswith('Summary and Contributions'): 
                    summary = node_text[i+1].strip()
                    if item:
                        yield item
                    item = Nips2020ReviewItem()
                    # strengths = ""
                    # weaknesses = ""
                    # clarity = ""
                    # correctness = ""
                    item['paper_name'] = response.meta['paper_name']
                    item['year'] = response.meta['year']
                    item['filename'] = response.meta['filename']

                    item['review_summary'] = summary

                if text.strip().startswith('Strengths'): 
                    strengths = node_text[i+1].strip()
                    item['review_strengths'] = strengths

                if text.strip().startswith('Weaknesses'): 
                    weaknesses = node_text[i+1].strip()
                    item['review_weaknesses'] = weaknesses
      
                if text.strip().startswith('Clarity'): 
                    clarity = node_text[i+1].strip()
                    item['review_clarity'] = clarity
      
                if text.strip().startswith('Correctness'): 
                    correctness = node_text[i+1].strip()
                    item['review_correctness'] = correctness
      
 

    # def get_paper_link(self, response):
    #     try:
    #         pdf_link = response.xpath("//div[@class='main-container']//a[text()='[PDF]']/@href").extract()[0]
    #     except:
    #         print("NO PDF")
    #         self.nopdf += 1
    #         yield None
    #     paper_title = response.xpath("//h2[@class='subtitle']/text()").extract()[0]
    #     year = response.meta['year']
    #     filename = "pdfs/I%s-%s.pdf"%(str(year)[2:],paper_title)
    #     # yield scrapy.Request(self.root_url + pdf_link, callback=self.download_paper, 
    #         # meta={'filename': filename})
    #     item = NipsScraperItem()
    #     item['name'] = paper_title
    #     item['year'] = year
    #     yield(item)
    
    # def download_paper(self, response):
    #     with open (response.meta['filename'], "wb") as pdff:
    #         pdff.write(response.body)
    #     self.nopdf
    #     print("NO PDFS", self.nopdf)