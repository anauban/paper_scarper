# -*- coding: utf-8 -*-
import scrapy
from nips_scraper.items import NipsScraperItem, NipsReviewItem

class NipsSpider(scrapy.Spider):
    name = 'reviews'
    allowed_domains = ['papers.neurips.cc', 'papers.nips.cc', 'media.neurips.cc', 'media.nips.cc', 
    'proceedings.neurips.cc']
    start_urls = ['https://proceedings.neurips.cc/']
    root_url = 'https://proceedings.neurips.cc'
    nopdf = 0

    def parse(self, response):
        print("In parse", response)
        links = response.xpath("//div[@class='container-fluid']//li/a/@href").extract()
        for link in links:
            if 'year' in response.meta:
                if link.split("/")[1] != 'paper':
                    continue
                yield scrapy.Request(self.root_url + link, callback = self.get_review_link, meta=response.meta)
            else: 
                year = int(link.split("/")[-1])
                # if year < 2013:
                if year <= 2019:
                    continue
                yield scrapy.Request(self.root_url + link, callback = self.parse, meta={'year': year})

    def get_review_link(self, response):
        print("Getting review link...")
        try:
            review_link = response.xpath("//div[@class='container-fluid']//a[text()='Review »']/@href").extract()[0]
        except:
            print("NO review")
        paper_title = response.xpath("//h2[@class='subtitle']/text()").extract()[0]
        year = response.meta['year']
        filename = "pdfs/I%s-%s.pdf"%(str(year)[2:],paper_title)
        print("REVIEW LINK", "https:" + review_link)
        yield scrapy.Request("https:" + review_link, callback = self.extract_reviews, meta={'year': year, 
            'paper_name': paper_title, 'filename': filename})

    def extract_reviews(self, response):
        print("GOT REQUEST")
        reviews = response.xpath("//div/text()").extract()
        if len(reviews) > 3:
            # It means it includes rebuttals
            
            # Get just the reviews (no rebuttals)
            all_nodes = response.xpath("//div[text()='Author Feedback']/preceding-sibling::*")
            if not all_nodes:
                all_nodes = response.xpath("//div")
            if not all_nodes:
                # all nodes with text
                all_nodes = response.xpath("//*[text()[string-length(normalize-space(.))>0]]") 

            reviewers=response.xpath("//*[starts-with(text(),'Submitted')]/text()").extract()
            if not reviewers:
                reviewers=response.xpath("//*[starts-with(text(),'Reviewer')]/text()").extract() 

            # Group review texts by reviewer
            reviews_texts = {r: [] for r in range(len(reviewers))}
            cur_reviewer = 0

            for node in all_nodes: 

                node_text = node.xpath("./text()").extract() 
                if node_text and cur_reviewer<len(reviewers) and node_text[0].strip().startswith(reviewers[cur_reviewer]): 
                    cur_reviewer=min(cur_reviewer+1,len(reviewers))
                # in case we need to skip a reviewer
                elif node_text and cur_reviewer<len(reviewers)-1 and node_text[0].strip().startswith(reviewers[cur_reviewer+1]): 
                    cur_reviewer=min(cur_reviewer+2,len(reviewers))

                else: 
                    if node.xpath('@class') and node.xpath('@class').extract()[0]=='response':
                        if cur_reviewer>0:
                            reviews_texts[cur_reviewer-1].append(node_text)



            if not reviews_texts[1]:
                reviews_texts = {r: [] for r in range(len(reviewers))}
                cur_reviewer = 0
                for node in all_nodes: 

                    node_text = node.xpath("./text()").extract() 
                    if node_text and cur_reviewer<len(reviewers) and node_text[0].strip().startswith(reviewers[cur_reviewer]): 
                        cur_reviewer=min(cur_reviewer+1,len(reviewers)) 
                    else: 
                        if cur_reviewer>0:
                            # Except now it also includes some titles like "Confidence" etc... only select <p> s?
                            reviews_texts[cur_reviewer-1].append(node_text)


            reviews = [" ".join(sum(reviews_texts[r],[])) for r in reviews_texts if reviews_texts[r]]
        print("REVIEWS", len(reviews))
        assert(len(reviews)>0)

        for review in reviews:
            item = NipsReviewItem()
            item['text'] = review
            item['paper_name'] = response.meta['paper_name']
            item['year'] = response.meta['year']
            item['filename'] = response.meta['filename']
            yield item

    # def get_paper_link(self, response):
    #     try:
    #         pdf_link = response.xpath("//div[@class='main-container']//a[text()='[PDF]']/@href").extract()[0]
    #     except:
    #         print("NO PDF")
    #         self.nopdf += 1
    #         yield None
    #     paper_title = response.xpath("//h2[@class='subtitle']/text()").extract()[0]
    #     year = response.meta['year']
    #     filename = "pdfs/I%s-%s.pdf"%(str(year)[2:],paper_title)
    #     # yield scrapy.Request(self.root_url + pdf_link, callback=self.download_paper, 
    #         # meta={'filename': filename})
    #     item = NipsScraperItem()
    #     item['name'] = paper_title
    #     item['year'] = year
    #     yield(item)
    
    # def download_paper(self, response):
    #     with open (response.meta['filename'], "wb") as pdff:
    #         pdff.write(response.body)
    #     self.nopdf
    #     print("NO PDFS", self.nopdf)