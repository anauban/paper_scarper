# -*- coding: utf-8 -*-
import scrapy
from nips_scraper.items import NipsScraperItem

class NipsSpider(scrapy.Spider):
    name = 'nips'
    allowed_domains = ['papers.neurips.cc']
    start_urls = ['https://papers.neurips.cc/']
    root_url = 'https://papers.neurips.cc/'
    nopdf = 0

    def parse(self, response):
        # links = response.xpath("//div[@class='main-container']//li/a/@href").extract()
        links = response.xpath("//div[@class='container-fluid']//li/a/@href").extract()

        for link in links:
            if 'year' in response.meta:
                if link.split("/")[1] != 'paper':
                    continue
                yield scrapy.Request(self.root_url + link, callback = self.get_paper_link, meta=response.meta)
            else:
                print("link", link)
                # year = int(link.split("-")[-1])
                year = int(link.split("/")[-1])
                if year > 2013:
                    continue
                yield scrapy.Request(self.root_url + link, callback = self.parse, meta={'year': year})

    def get_paper_link(self, response):
        try:
            # pdf_link = response.xpath("//div[@class='main-container']//a[text()='[PDF]']/@href").extract()[0]
            pdf_link = response.xpath("//div[@class='container-fluid']//a[starts-with(text(),'Paper')]/@href").extract()[0]
        except:
            print("NO PDF")
            self.nopdf += 1
            yield None
        # paper_title = response.xpath("//h2[@class='subtitle']/text()").extract()[0]
        paper_title = response.xpath("//h4/text()").extract()[0]
        year = response.meta['year']
        # filename = "pdfs_2020/I%s-%s.pdf"%(str(year)[2:],paper_title)
        # yield scrapy.Request(self.root_url + pdf_link, callback=self.download_paper, 
            # meta={'filename': filename})
        item = NipsScraperItem()
        item['name'] = paper_title
        item['year'] = year
        yield(item)
    
    def download_paper(self, response):
        with open (response.meta['filename'], "wb") as pdff:
            pdff.write(response.body)
        self.nopdf
        print("NO PDFS", self.nopdf)