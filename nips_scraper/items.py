# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NipsScraperItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    name = scrapy.Field()
    year = scrapy.Field()


class NipsReviewItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    text = scrapy.Field()
    paper_name = scrapy.Field()
    paper_id = scrapy.Field()
    year = scrapy.Field()
    filename = scrapy.Field()

class Nips2020ReviewItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    review_summary = scrapy.Field()
    review_strengths = scrapy.Field()
    review_weaknesses = scrapy.Field()
    review_correctness = scrapy.Field()
    review_clarity = scrapy.Field()
    paper_name = scrapy.Field()
    paper_id = scrapy.Field()
    year = scrapy.Field()
    filename = scrapy.Field()