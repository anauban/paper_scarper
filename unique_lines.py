import sys
import json

papers = {}
with open(sys.argv[1]) as f:
    for line in f:
        content = json.loads(line)
        if content['paper_name'] not in papers:
            papers[content['paper_name']] = []
        papers[content['paper_name']].append(content)

unique_texts = {}
for paper, content in papers.items():
    texts = set([c['text'] for c in content])
    unique_texts[paper] = []
    for t in texts:
        new_item = {f: content[0][f] for f in content[0]}
        new_item['text'] = t
        unique_texts[paper].append(new_item)

# get the unique values

for paper, items in unique_texts.items():
    for item in items:
        print(json.dumps(item))