# -*- coding: utf-8 -*-
import scrapy
from acl.items import AclItem

class CrawlYearSpider(scrapy.Spider):
    name = 'crawl_year'
    allowed_domains = ['www.aclweb.org']
    start_urls = ['https://www.aclweb.org/anthology/venues/coling//']
    root_url = "https://www.aclweb.org/"

    def parse(self, response):
        sections = response.xpath(".//div[@class='row']")
        for section in sections:
            year = section.xpath(".//div[@class='col-sm-1']//text()").extract()[0]
            # if int(year) >= 2014:
                # continue
            papers = section.xpath(".//div[@class='col-sm']/ul/li//a/@href").extract()
            for page in papers:
                print("Scraping", page)
                yield scrapy.Request(self.root_url+page, callback=self.download_papers, meta={'year':year})

    def download_papers(self, response):
        print("Downloading papers")
        papers = response.xpath(".//p/span[@class='mr-2']/a[@class='badge badge-primary align-middle mr-1']/@href").extract()
        for paper in papers:
            name_year = paper.split("/")[-1].split("-")[0][1:]
            assert(name_year==str(response.meta['year'])[2:])
            item = AclItem()
            item['url'] = paper
            yield(item)
