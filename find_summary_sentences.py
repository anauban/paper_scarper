import json
import re
from nltk import sent_tokenize
import numpy as np
reviews = []
with open('reviews_texts3_uniquniq.jl') as f:
    for line in f:
        reviews.append(json.loads(line.strip()))

phrase_counts = {'the paper': 0, 'this paper': 0, 'this article': 0, 'rebuttal': 0, 'response': 0, '\\-\\-\\-': 0, '===': 0, '~~~': 0}
phrase_counts_perart = {'the paper': 0, 'this paper': 0, 'this article': 0, 'rebuttal': 0, 'response': 0, '\\-\\-\\-': 0, '===': 0, '~~~': 0}
phrase_counts_persent = {'the paper': [], 'this paper': [], 'this article': [], 'rebuttal': [], 'response': [], '\\-\\-\\-': [], '===': [], '~~~': []}


for review in reviews:
    sentences = sent_tokenize(review['text'].lower())
    found = {p : False for p in phrase_counts}
    for phrase in phrase_counts:

        for s, sentence in enumerate(sentences):
            counts = re.findall(phrase, sentence)
            phrase_counts[phrase] += len(counts)
            if len(counts):
                found[phrase] = True
                phrase_counts_persent[phrase].append(s)
            # break
        if found[phrase]:
            phrase_counts_perart[phrase] += 1


print("overall counts", phrase_counts)
print("nr articles counts", phrase_counts_perart)
# print(phrase_counts_persent)
print("per sentence counts", {p: len(phrase_counts_persent[p]) for p in phrase_counts_persent})
print("median sentence", {p: np.median(phrase_counts_persent[p]) for p in phrase_counts_persent})
